import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './home.page';

const routes: Routes = [
  {
    path: '',
    component: HomePage,
    children:[
  {
    path: 'all',
    loadChildren: () => import('./all/all.module').then( m => m.AllPageModule)
  },
  {
    path: 'friend',
    loadChildren: () => import('./friend/friend.module').then( m => m.FriendPageModule)
  },
  {
    path: 'familly',
    loadChildren: () => import('./familly/familly.module').then( m => m.FamillyPageModule)
  }
]
},
{
  path:'',
  redirectTo:'/home/all',
  pathMatch:'full'
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomePageRoutingModule {}
